﻿using System.Collections.Generic;
using UnityEngine;

public class BezierPath : Path {
	private Dictionary<Track.TrackPoint, (Track.TrackPoint, Track.TrackPoint)> anchorToControlPointMap;

	public BezierPath(Track.TrackPoint start, Track.TrackPoint end) : base(start) {
		anchorToControlPointMap = new Dictionary<Track.TrackPoint, (Track.TrackPoint, Track.TrackPoint)>();
		Vector2 dir = (end.Location - start.Location).normalized;
		anchorToControlPointMap.Add(start, (null, new Track.TrackPoint(start.Location + dir)));
		anchorToControlPointMap.Add(end, (new Track.TrackPoint(end.Location - dir), null));
	}

	public override void InsertAnchorPoint(Track.TrackPoint anchorPoint) {
		var prevPoint = FindPointBeforeGivenX(anchorPoint.Location.x);
		var nextPoint = prevPoint.NextPoint;

		anchorPoint.SetPreviousPoint(prevPoint);
		anchorPoint.SetNextPoint(prevPoint.NextPoint);
		prevPoint.NextPoint.SetPreviousPoint(anchorPoint);
		prevPoint.SetNextPoint(anchorPoint);

		anchorPoint.OnEdit += (p, d) => AutoSetAffectedControlPoints(p);
		AutoSetAffectedControlPoints(anchorPoint);
	}

	private void AutoSetControlPoints(Track.TrackPoint anchorPoint) {
		Track.TrackPoint prevControl = null;
		Track.TrackPoint nextControl = null;

		var prevPoint = anchorPoint.PrevPoint;
		var nextPoint = anchorPoint.NextPoint;

		Vector2 dir = Vector2.zero;
		Vector2 offset;
		float[] neighborDistances = new float[2];
		if (prevPoint != null) {
			offset = prevPoint.Location - anchorPoint.Location;
			dir += offset.normalized;
			neighborDistances[0] = offset.magnitude;
		}
		if (nextPoint != null) {
			offset = nextPoint.Location - anchorPoint.Location;
			dir -= offset.normalized;
			neighborDistances[1] = -offset.magnitude;
		}
		dir.Normalize();
	
		if (prevPoint != null) {
			Vector2 newPrevControl = anchorPoint.Location + dir * neighborDistances[0] * 0.5f;
			prevControl = new Track.TrackPoint(newPrevControl);
		}
		if (nextPoint != null) {
			Vector2 newNextControl = anchorPoint.Location + dir * neighborDistances[1] * 0.5f;
			nextControl = new Track.TrackPoint(newNextControl);
		}

		if (!anchorToControlPointMap.ContainsKey(anchorPoint)) {
			anchorToControlPointMap.Add(anchorPoint, (prevControl, nextControl));
		}
		else {
			anchorToControlPointMap[anchorPoint] = (prevControl, nextControl);
		}
	}
	private void AutoSetAffectedControlPoints(Track.TrackPoint anchorPoint) {
		AutoSetControlPoints(anchorPoint.PrevPoint);
		AutoSetControlPoints(anchorPoint);
		AutoSetControlPoints(anchorPoint.NextPoint);
	}

	public override void RemoveAnchorPoint(Track.TrackPoint anchorPoint) {
		var prevPoint = anchorPoint.PrevPoint;
		var nextPoint = anchorPoint.NextPoint;

		base.RemoveAnchorPoint(anchorPoint);
		anchorToControlPointMap.Remove(anchorPoint);

		AutoSetControlPoints(prevPoint);
		AutoSetControlPoints(nextPoint);
	}

	public override Vector2 Evaluate(float dist) {
		var prevPoint = FindPointBeforeGivenX(dist);
		var nextPoint = prevPoint.NextPoint;
		if (nextPoint == null) {
			return prevPoint.Location;
		}
		
		Vector2 a = prevPoint.Location;
		Vector2 b = anchorToControlPointMap[prevPoint].Item2.Location;
		Vector2 c = anchorToControlPointMap[nextPoint].Item1.Location;
		Vector2 d = nextPoint.Location;

		float t = (dist - a.x) / (d.x - a.x);

		float minus = 1 - t;
		float minus2 = minus * minus;
		float minus3 = minus * minus * minus;

		Vector2 bez = (minus3 * a) + (3 * minus2 * t * b) + (3 * minus * t * t * c) + (t * t * t * d);

		return new Vector2(dist, bez.y);
	}
}
