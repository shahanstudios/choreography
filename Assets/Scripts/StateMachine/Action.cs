﻿public abstract class Action<T> {
	public virtual void BeginAction(T context) { }
	public abstract void Invoke(T context);
	public virtual void EndAction(T context) { }

	public static implicit operator Action<T>(System.Action<T> func) => new DelegateAction<T>(func);
}
