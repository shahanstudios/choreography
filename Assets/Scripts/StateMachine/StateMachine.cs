﻿using System.Linq;

public class StateMachine<T>
{
	private T owner;
	private State<T>[] states;
	private State<T> currentState;

	public StateMachine(T owner, params State<T>[] states) {
		this.owner = owner;
		this.states = states;
		foreach (State<T> state in states) {
			state.OnTransitionToNewStateEvent += (newState, context) => {
				currentState.OnExit(context);
				newState.OnEnter(context);
			};
			state.OnTransitionToNewStateEvent += (newState, context) => currentState = newState;
		}
		this.currentState = states.First();
		this.currentState.OnEnter(owner);
	}

	public void TransitionToState(State<T> newState) {
		currentState.OnExit(this.owner);
		newState.OnEnter(this.owner);
		currentState = newState;
	}

	public void Update() {
		currentState?.Update(owner);
	}
	public void UpdatePhysics() {
		currentState?.UpdatePhysics(owner);
	}
}
