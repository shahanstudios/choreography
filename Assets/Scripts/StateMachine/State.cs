﻿using System.Collections.Generic;

public class State<T> {
	public readonly string ID;

	private List<Action<T>> actions = new List<Action<T>>();
	private List<Action<T>> physicsActions = new List<Action<T>>();
	private List<Transition<T>> transitions = new List<Transition<T>>();
	private List<Transition<T>> physicsTransitions = new List<Transition<T>>();

	public virtual void OnEnter(T context) {
		foreach (Action<T> action in actions) {
			action.BeginAction(context);
		}
		if (OnStateEnterEvent != null) {
			OnStateEnterEvent.Invoke(context);
		}
	}
	public virtual void OnExit(T context) {
		foreach (Action<T> action in actions) {
			action.EndAction(context);
		}
		if (OnStateExitEvent != null) {
			OnStateExitEvent.Invoke(context);
		}
	}

	public event System.Action<T> OnStateEnterEvent;
	public event System.Action<T> OnStateExitEvent;
	public event System.Action<State<T>, T> OnTransitionToNewStateEvent;

	public State(string ID) {
		this.ID = ID;
	}

	public State<T> AddAction(params Action<T>[] action) {
		actions.AddRange(action);
		return this;
	}
	public State<T> AddTransition(params Transition<T>[] transition) {
		transitions.AddRange(transition);
		return this;
	}

	public void Update(T context) {
		foreach (Action<T> action in actions) {
			action.Invoke(context);
		}
		EvaluateTransitions(transitions, context);
	}
	public void UpdatePhysics(T context) {
		foreach (Action<T> action in physicsActions) {
			action.Invoke(context);
		}
		EvaluateTransitions(physicsTransitions, context);
	}
	private void EvaluateTransitions(List<Transition<T>> transitions, T context) {
		foreach (Transition<T> transition in transitions) {
			bool conditionSucceeded = transition.condition.Evaluate(context);
			if (conditionSucceeded) {
				if (OnTransitionToNewStateEvent != null) {
					OnTransitionToNewStateEvent.Invoke(transition.trueState, context);
				}
			}
		}
	}

	public override string ToString() {
		return $"State: {ID}";
	}
}
