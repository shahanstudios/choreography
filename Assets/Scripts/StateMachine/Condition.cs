﻿public abstract class Condition<T> {
	public abstract bool Evaluate(T context);

	public static implicit operator Condition<T>(System.Predicate<T> func) => new DelegateCondition<T>(func);
}
