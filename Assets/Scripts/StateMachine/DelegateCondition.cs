﻿public class DelegateCondition<T> : Condition<T> {
	private System.Predicate<T> delegateFunc;

	public DelegateCondition(System.Predicate<T> delegateFunc) {
		this.delegateFunc = delegateFunc;
	}
	public override bool Evaluate(T context) {
		return delegateFunc.Invoke(context);
	}
}
