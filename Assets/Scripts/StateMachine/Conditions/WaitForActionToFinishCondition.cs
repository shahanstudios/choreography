﻿public class WaitForActionToFinishCondition<T> : Condition<T> {
	private bool isActionFinished;
	public WaitForActionToFinishCondition(FinishableAction<T> actionToWaitFor) {
		actionToWaitFor.OnActionFinishedEvent += (context) => isActionFinished = true;
	}
	public override bool Evaluate(T context) {
		if (isActionFinished) {
			isActionFinished = false;
			return true;
		}
		return false;
	}
}
