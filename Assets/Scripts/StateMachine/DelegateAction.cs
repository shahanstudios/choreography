﻿public class DelegateAction<T> : Action<T> {
	private System.Action<T> delegateFunc;
	public DelegateAction(System.Action<T> delegateFunc) {
		this.delegateFunc = delegateFunc;
	}
	public override void Invoke(T context) {
		delegateFunc.Invoke(context);
	}
}
