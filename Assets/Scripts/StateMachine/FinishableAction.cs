﻿public abstract class FinishableAction<T> : Action<T> {
	public event System.Action<T> OnActionFinishedEvent;

	public void FireFinishedEvent(T context) {
		if (OnActionFinishedEvent != null) {
			OnActionFinishedEvent.Invoke(context);
		}
	}
}
