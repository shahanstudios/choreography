﻿public class Transition<T> {
	public Condition<T> condition;
	public State<T> trueState;

	public Transition(Condition<T> condition, State<T> trueState) {
		this.condition = condition;
		this.trueState = trueState;
	}
}
