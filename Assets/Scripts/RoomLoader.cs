﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RoomLoader : MonoBehaviour {
	private static RoomLoader instance;
	public static RoomLoader Instance {
		get {
			if (instance == null) {
				GameObject roomLoader = new GameObject();
				roomLoader.name = "_RoomLoader";
				instance = roomLoader.AddComponent<RoomLoader>();
			}
			return instance;
		}
	}
	public static RoomLoader RawInstance => instance;

	private void Awake() {
		if (instance != null && instance != this) {
			Destroy(this.gameObject);
		}
		else if (instance == null) {
			instance = this;
		}
	}
	private void Start() {
		string playerAssetsSceneName = "PlayerAssets";
		if (!SceneManager.GetSceneByName(playerAssetsSceneName).isLoaded) {
			StartCoroutine(LoadPlayerAssets(playerAssetsSceneName));
		}
		DontDestroyOnLoad(this.gameObject);
	}

	public event System.Action<Room> OnLoadRoomEvent;
	public event System.Action<Room> OnUnloadRoomEvent;

	private IEnumerator LoadPlayerAssets(string playerAssetsSceneName) {
		AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(playerAssetsSceneName, LoadSceneMode.Additive);
		string roomName = $"Rooms/{SceneManager.GetActiveScene().name}";
		ResourceRequest loadRoomData = Resources.LoadAsync<Room>(roomName);

		while (!asyncLoad.isDone || !loadRoomData.isDone) {
			yield return null;
		}

		Room r = loadRoomData.asset as Room;

		Scene playerAssetsScene = SceneManager.GetSceneByName(playerAssetsSceneName);
		GameObject[] playerAssetsGameObjects = playerAssetsScene.GetRootGameObjects();
		foreach (GameObject playerAssetGO in playerAssetsGameObjects) {
			if (playerAssetGO.TryGetComponent(typeof(CameraBounds), out Component boundsComponent)) {
				(boundsComponent as CameraBounds).SetRoomBounds(r.Bounds);
			}
			if (playerAssetGO.TryGetComponent(typeof(PlayerMovement), out Component playerMovementComponent)) {
				playerAssetGO.transform.position = r.DefaultPlayerLocation;
			}
		}
	}

	public IEnumerator Load(Room room) {
		AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(room.SceneName, LoadSceneMode.Additive);

		while (!asyncLoad.isDone) {
			yield return null;
		}

		OnLoadRoomEvent?.Invoke(room);
	}
	public IEnumerator Unload(Room room) {
		AsyncOperation asyncUnload = SceneManager.UnloadSceneAsync(room.SceneName, UnloadSceneOptions.None);

		while (!asyncUnload.isDone) {
			yield return null;
		}

		OnUnloadRoomEvent?.Invoke(room);
	}
}
