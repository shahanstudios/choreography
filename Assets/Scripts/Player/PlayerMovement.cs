﻿using UnityEngine;

[RequireComponent(typeof(PlayerPhysicsBody))]
public class PlayerMovement : MonoBehaviour {

	[SerializeField] private float distanceToFreefall = 4f;
	[SerializeField] private float timeToCompleteFreefall = 1f;
	[SerializeField] private float jumpHeight = 1f;
	[SerializeField] private float timeToJumpApex = 0.25f;
	private float gravity;
	private float jumpVelocity;

	[SerializeField]
	private float speed = 5.0f;

	private Track movementTrack;
	public Track MovementTrack => movementTrack;
	private Track jumpingTrack;
	public Track JumpingTrack => jumpingTrack;

	private float movementTiming = 0.0f;
	private Vector2 originalPosition;

	private PlayerPhysicsBody physicsBody;

	private bool wasGrounded;
	private bool isGrounded => physicsBody.Collisions.below;
	private bool hasInitiatedJump;
	private bool hasLanded => hasInitiatedJump && isGrounded;
	private bool reachedZeroBeforeTouchdown;

	private void Awake() {
		GameStateManager.Instance.OnExecutionStateEnter += () => originalPosition = transform.position;
		GameStateManager.Instance.OnPlanningStateEnter += () => {
			transform.position = originalPosition;
			movementTiming = 0.0f;
			physicsBody.Reset();
			hasInitiatedJump = false;
			reachedZeroBeforeTouchdown = false;
		};

		physicsBody = GetComponent<PlayerPhysicsBody>();

		gravity = -(distanceToFreefall * 2)/(timeToCompleteFreefall * timeToCompleteFreefall);
		jumpVelocity = (jumpHeight - (0.5f * gravity * timeToJumpApex * timeToJumpApex)) / timeToJumpApex;

		movementTrack = new Track(-1, 1, 0, 15f, new Vector2(0.5f, 0.25f));
		movementTrack.InitializePath();
		float jumpingTrackHeight = 2 / timeToJumpApex;
		jumpingTrack = new JumpTrack(0f, jumpingTrackHeight, 0f, 15f, 1f, timeToJumpApex);
		jumpingTrack.InitializePath(false);

		RoomLoader.Instance.OnLoadRoomEvent += (room) => {
			movementTrack.Clear();
			jumpingTrack.Clear();
		};
	}

	private void FixedUpdate() {
		if (GameStateManager.Instance.CurrentGameState.State == GameStateManager.State.Executing) {
			movementTiming += Time.deltaTime;

			Vector2 movementFactor = GetMovementFactor();
			Vector2 xVelocity = Vector2.right * movementFactor.x * speed;
			Vector2 yVelocity = Vector2.up * movementFactor.y * jumpVelocity;

			physicsBody.SetVelocity(new Vector2(xVelocity.x, physicsBody.Velocity.y));
			if (!reachedZeroBeforeTouchdown) {
				reachedZeroBeforeTouchdown = hasInitiatedJump && !hasLanded && yVelocity.y == 0;
			}
			if (reachedZeroBeforeTouchdown && isGrounded && yVelocity.y == 0) {
				reachedZeroBeforeTouchdown = false;	
			}
			if (!reachedZeroBeforeTouchdown) {
				physicsBody.ApplyVelocity(yVelocity);
			}

			physicsBody.SetAcceleration(Vector2.up * gravity);
			if (hasInitiatedJump && hasLanded) {
				hasInitiatedJump = false;
			}
			if (!hasInitiatedJump) {
				hasInitiatedJump = wasGrounded && !isGrounded;
			}
		}
		wasGrounded = isGrounded;
	}

	private Vector2 GetMovementFactor() {
		float xFactor = movementTrack.TrackPath.Evaluate(movementTiming).y;
		float yFactor = jumpingTrack.TrackPath.Evaluate(movementTiming).y;

		return new Vector2(xFactor, yFactor);
	}

}
