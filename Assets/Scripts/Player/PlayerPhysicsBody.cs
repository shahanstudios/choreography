﻿using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class PlayerPhysicsBody : MonoBehaviour {
	[SerializeField]
	private LayerMask collisionMask;

	private Vector2 velocity;
	public Vector2 Velocity => velocity;
	private Vector2 acceleration;

	const float skinWidth = 0.05f;
	private int horizontalRayCount = 4;
	private float horizontalRaySpacing;
	private int verticalRayCount = 4;
	private float verticalRaySpacing;

	private new Collider2D collider;
	private Bounds skinnedBounds {
		get {
			Bounds bounds = collider.bounds;
			bounds.Expand(skinWidth * -2);
			return bounds;
		}
	}
	private RaycastOrigins origins;
	private CollisionInfo collisions;
	public CollisionInfo Collisions => collisions;
	
	private void Awake() {
		collider = GetComponent<Collider2D>();
		CalculateRaySpacing();
		UpdateRaycastOrigins();
	}

	private void FixedUpdate() {
		UpdateRaycastOrigins();

		Vector2 displacement = (velocity * Time.deltaTime) + (0.5f * acceleration * Time.deltaTime * Time.deltaTime);
		if (displacement != Vector2.zero) {
			ResolveCollisions(ref displacement);
			transform.Translate(displacement);
		}
		velocity += acceleration * Time.deltaTime;

		if (collisions.below || collisions.above) {
			velocity = Vector2.right * velocity.x;
		}
		if (collisions.left || collisions.right) {
			velocity = Vector2.up * velocity.y;
		}
	}

	public void ApplyVelocity(Vector2 velocity) {
		//v = v0 + v;
		Vector2 newVelocity = this.velocity + velocity * Time.deltaTime;
		this.velocity = newVelocity;
	}
	public void SetVelocity(Vector2 velocity) {
		this.velocity = velocity;
	}
	public void SetAcceleration(Vector2 acceleration) {
		this.acceleration = acceleration;
	}

	public void Reset() {
		this.velocity = Vector2.zero;
		this.acceleration = Vector2.zero;
		collisions.Reset();
	}

	private void ResolveCollisions(ref Vector2 displacement) {
		collisions.Reset();
		ResolveVerticalCollisions(ref displacement);
		ResolveHorizontalCollisions(ref displacement);
	}
	private void ResolveVerticalCollisions(ref Vector2 displacement) {
		float dirY = Mathf.Sign(displacement.y);
		float rayLength = Mathf.Abs(displacement.y) + skinWidth;

		for (int i = 0; i < verticalRayCount; i++) {
			Vector2 rayOrigin = dirY == -1 ? origins.bottomLeft : origins.topLeft;
			rayOrigin += Vector2.right * (verticalRaySpacing * i) + displacement;
			RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.up * dirY, rayLength, collisionMask);

			if (hit) {
				displacement.y = (hit.distance - skinWidth) * dirY;
				rayLength = hit.distance;

				collisions.above = dirY == 1;
				collisions.below = dirY == -1;
			}
			Debug.DrawRay(rayOrigin, Vector2.up * rayLength * dirY, Color.green);
		}
	}
	private void ResolveHorizontalCollisions(ref Vector2 displacement) {
		float dirX = Mathf.Sign(displacement.x);
		float rayLength = Mathf.Abs(displacement.x) + skinWidth;

		for (int i = 0; i < horizontalRayCount; i++) {
			Vector2 rayOrigin = dirX == -1 ? origins.bottomLeft : origins.bottomRight;
			rayOrigin += Vector2.up * (horizontalRaySpacing * i) + displacement;
			RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.right * dirX, rayLength, collisionMask);

			if (hit) {
				displacement.x = (hit.distance - skinWidth) * dirX;
				rayLength = hit.distance;

				collisions.right = dirX == 1;
				collisions.left = dirX == -1;
			}
			Debug.DrawRay(rayOrigin, Vector2.right * rayLength * dirX, Color.green);
		}
	}

	private void UpdateRaycastOrigins() {
		Bounds bounds = skinnedBounds;
		origins.bottomLeft = new Vector2(bounds.min.x, bounds.min.y);
		origins.bottomRight = new Vector2(bounds.max.x, bounds.min.y);
		origins.topLeft = new Vector2(bounds.min.x, bounds.max.y);
		origins.topRight = new Vector2(bounds.max.x, bounds.max.y);
	}
	private void CalculateRaySpacing() {
		horizontalRayCount = Mathf.Clamp(horizontalRayCount, 2, 10);
		verticalRayCount = Mathf.Clamp(verticalRayCount, 2, 10);

		Bounds bounds = skinnedBounds;
		horizontalRaySpacing = bounds.size.y / (horizontalRayCount - 1);
		verticalRaySpacing = bounds.size.x / (verticalRayCount - 1);
	}

	public struct RaycastOrigins {
		public Vector2 topLeft, topRight;
		public Vector2 bottomLeft, bottomRight;
	}

	[System.Serializable]
	public struct CollisionInfo {
		public bool above, below;
		public bool left, right;

		public void Reset() {
			above = below = false;
			left = right = false;
		}
	}
}
