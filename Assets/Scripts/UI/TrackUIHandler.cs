﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

[RequireComponent(typeof(GraphicRaycaster))]
[RequireComponent(typeof(RectTransform))]
[RequireComponent(typeof(TrackUI))]
public class TrackUIHandler : MonoBehaviour {
	[SerializeField] private Camera trackCam = null;
	[SerializeField] private Image cursorImage = null;
	[SerializeField] private EventSystem eventSystem = null;
	[SerializeField] private float cursorSpeed = 5f;

	private GraphicRaycaster raycaster;
	private RectTransform trackTransform;
	private RectTransform cursorTransform;

	private TrackUI trackUI;
	private Track track;
	private Track Track {
		get {
			if (track == null) {
				track = trackUI.Track;
			}
			return track;
		}
	}

	private TimelineControls timelineControls;
	private bool applyCursorMove;
	private InputAction cursorMoveAction;

	private Vector2[] trackScreenCorners;

	private Vector2 cursorScreenPosition;
	private Vector2 CursorScreenPosition {
		get { return cursorScreenPosition; }
		set {
			cursorScreenPosition = value;

			Vector2 minCorner = trackScreenCorners[0];
			Vector2 maxCorner = trackScreenCorners[1];
			cursorScreenPosition = new Vector2(
				Mathf.Clamp(cursorScreenPosition.x, minCorner.x, maxCorner.x),
				Mathf.Clamp(cursorScreenPosition.y, minCorner.y, maxCorner.y)
			);

			OnCursorPositionChange();
		}
	}

	private bool isSelected;
	private bool shouldActivate {
		get {
			return isSelected && GameStateManager.Instance.CurrentGameState.State == GameStateManager.State.Planning;
		}
	}

	private TrackPointUIHandler selectedTrackPoint;

	private void Awake() {
		raycaster = GetComponentInParent<GraphicRaycaster>();
		trackTransform = GetComponent<RectTransform>();
		cursorTransform = cursorImage.GetComponent<RectTransform>();
		trackUI = GetComponent<TrackUI>();
		timelineControls = new TimelineControls();

		cursorMoveAction = timelineControls.Track.MoveCursor;

		cursorImage.enabled = isSelected;
		GameStateManager.Instance.OnExecutionStateEnter += () =>
		{
			cursorImage.enabled = false;
		};
		GameStateManager.Instance.OnPlanningStateEnter += () =>
		{
			if (isSelected) {
				cursorImage.enabled = true;
			}
		};

	}
	private void Start() {
		trackScreenCorners = new Vector2[2];
		Vector3[] worldCorners = new Vector3[4];
		trackTransform.GetWorldCorners(worldCorners);
		for (int i = 0; i < worldCorners.Length / 2; i++) {
			trackScreenCorners[i] = trackCam.WorldToScreenPoint(worldCorners[i * 2]);
		}
	}

	private void OnEnable() {
		timelineControls.Enable();
		timelineControls.Track.MoveMouse.performed += OnMouseMove;
		timelineControls.Track.MoveCursor.performed += UpdateShouldApplyCursorMove;
		timelineControls.Track.MoveCursor.canceled += UpdateShouldApplyCursorMove;
		timelineControls.Track.TrackInteract.performed += OnTrackInteract;
		timelineControls.Track.RemovePoint.performed += OnRemovePoint;
	}
	private void OnDisable() {
		timelineControls.Disable();
		timelineControls.Track.MoveMouse.performed -= OnMouseMove;
		timelineControls.Track.MoveCursor.performed -= UpdateShouldApplyCursorMove;
		timelineControls.Track.MoveCursor.canceled -= UpdateShouldApplyCursorMove;
		timelineControls.Track.TrackInteract.performed -= OnTrackInteract;
		timelineControls.Track.RemovePoint.performed -= OnRemovePoint;
	}
	public void Select() {
		isSelected = true;
		cursorImage.enabled = shouldActivate;
	}
	public void Deselect() {
		isSelected = false;
		cursorImage.enabled = shouldActivate;
	}

	private void Update() {
		if (applyCursorMove) {
			OnCursorMove(cursorMoveAction.ReadValue<Vector2>());
		}
	}

	private void OnMouseMove(InputAction.CallbackContext context) {
		if (!shouldActivate) {
			return;
		}

		Vector2 originalCursorPos = GetLocalTrackCursorPosition();
		CursorScreenPosition = context.ReadValue<Vector2>();
		Vector2 delta = GetLocalTrackCursorPosition() - originalCursorPos;
		if (selectedTrackPoint) {
			selectedTrackPoint.OnMove(delta);
		}
	}

	private void UpdateShouldApplyCursorMove(InputAction.CallbackContext context) {
		applyCursorMove = context.phase == InputActionPhase.Performed;
	}
	private void OnCursorMove(Vector2 delta) {
		if (!shouldActivate) {
			return;
		}

		Vector2 originalCursorPos = GetLocalTrackCursorPosition();
		CursorScreenPosition += delta * cursorSpeed;
		delta = GetLocalTrackCursorPosition() - originalCursorPos;
		if (selectedTrackPoint) {
			selectedTrackPoint.OnMove(delta);
		}
	}
	private void OnCursorPositionChange() {
		Vector2 localPoint = GetLocalTrackCursorPosition();

		Rect trackRect = trackTransform.rect;
		localPoint = new Vector2(
				Mathf.Clamp(localPoint.x, trackRect.xMin, trackRect.xMax),
				Mathf.Clamp(localPoint.y, trackRect.yMin + 1, trackRect.yMax - 1));

		cursorTransform.anchoredPosition = localPoint;
	}
	private GameObject GetClickedObject() {
		List<RaycastResult> results = new List<RaycastResult>();
		if (!shouldActivate) {
			return null;
		}

		PointerEventData pointerData = new PointerEventData(eventSystem);
		pointerData.position = trackCam.WorldToScreenPoint(cursorTransform.position);
		raycaster.Raycast(pointerData, results);
		return results.Count > 0 ? results[0].gameObject : null;
	}
	private void OnTrackInteract(InputAction.CallbackContext context) {
		if (selectedTrackPoint) {
			selectedTrackPoint = null;
			return;
		}

		GameObject clickedObject = GetClickedObject();
		if (clickedObject) {
			if (clickedObject == this.gameObject) {
				trackUI.AddTrackPoint(GetLocalTrackCursorPosition());
			}
			else if (clickedObject.TryGetComponent(out TrackPointUIHandler pointUIHandler)) {
				selectedTrackPoint = pointUIHandler;
			}
		}
	}
	private void OnRemovePoint(InputAction.CallbackContext context) {
		if (selectedTrackPoint) {
			selectedTrackPoint.RemovePoint();
			selectedTrackPoint = null;
			return;
		}

		GameObject clickedObject = GetClickedObject();
		if (clickedObject && clickedObject.TryGetComponent(out TrackPointUIHandler pointUIHandler)) {
			pointUIHandler.RemovePoint();
		}
	}

	private Vector2 GetLocalTrackCursorPosition() {
		RectTransformUtility.ScreenPointToLocalPointInRectangle(trackTransform, cursorScreenPosition, trackCam, out Vector2 localPoint);
		Rect trackRect = trackTransform.rect;
		localPoint = new Vector2(
				Mathf.Clamp(localPoint.x, trackRect.xMin, trackRect.xMax),
				Mathf.Clamp(localPoint.y, trackRect.yMin, trackRect.yMax));
		localPoint.x = trackUI.xTrackPosToXRectPosNormalizeFunc(trackUI.xRectPosToXTrackPosNormalizeFunc(localPoint.x));
		localPoint.y = trackUI.yTrackPosToYRectPosNormalizeFunc(trackUI.yRectPosToYTrackPosNormalizeFunc(localPoint.y));

		return localPoint;
	}
}
