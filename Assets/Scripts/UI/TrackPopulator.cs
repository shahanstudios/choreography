﻿using UnityEngine;

public abstract class TrackPopulator : MonoBehaviour {
	public abstract Track GetTrack();
}
