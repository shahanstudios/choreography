﻿using UnityEngine;
using UnityEngine.InputSystem;

public class TrackUIManager : MonoBehaviour {
	private TrackUIHandler[] trackHandlers;
	private int selectedIndex;

	private TimelineControls timelineControls;
	private void Awake() {
		trackHandlers = GetComponentsInChildren<TrackUIHandler>();
		UpdateSelectedTrack(0);
		timelineControls = new TimelineControls();
	}
	private void OnEnable() {
		timelineControls.Enable();
		timelineControls.Track.SelectTrack.performed += HandleSelectTrack;
	}
	private void OnDisable() {
		timelineControls.Disable();
		timelineControls.Track.SelectTrack.performed -= HandleSelectTrack;
	}

	private void HandleSelectTrack(InputAction.CallbackContext context) {
		int dir = Mathf.RoundToInt(context.ReadValue<float>());
		int newSelection = (selectedIndex - dir);
		if (newSelection < 0) {
			newSelection = trackHandlers.Length - 1 + (newSelection + 1);
		}
		else {
			newSelection %= trackHandlers.Length;
		}
		UpdateSelectedTrack(newSelection);
	}

	private void UpdateSelectedTrack(int newSelection) {
		selectedIndex = newSelection;
		SelectTrack();
		DeselectTracks();
	}
	private void SelectTrack() {
		trackHandlers[selectedIndex].gameObject.SetActive(true);
		trackHandlers[selectedIndex].Select();
	}
	private void DeselectTracks() {
		for (int i = 0; i < trackHandlers.Length; i++) {
			if (i != selectedIndex) {
				trackHandlers[i].gameObject.SetActive(false);
				trackHandlers[i].Deselect();
			}
		}
	}
}
