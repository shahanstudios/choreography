﻿using UnityEngine;

public class TrackPopulator_PlayerMovement : TrackPopulator {
	public override Track GetTrack() {
		return GameObject.FindObjectOfType<PlayerMovement>().MovementTrack;
	}
}
