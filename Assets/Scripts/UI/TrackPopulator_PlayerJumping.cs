﻿using UnityEngine;

public class TrackPopulator_PlayerJumping : TrackPopulator {
	public override Track GetTrack() {
		return GameObject.FindObjectOfType<PlayerMovement>().JumpingTrack;
	}
}
