﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour {
	public void Resume() {
		GameStateManager.Instance.Resume();
	}
	public void Quit() {
		SceneManager.LoadScene("MainMenu");
	}
}
