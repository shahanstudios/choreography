﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {
	public void Start() {
		Destroy(RoomLoader.RawInstance?.gameObject);
		Destroy(GameStateManager.RawInstance?.gameObject);
	}
	public void LoadNewGame() {
		SceneManager.LoadScene("Room00");
	}
	public void Quit() {
		Application.Quit();
	}
}
