﻿using UnityEngine;
using UnityEngine.EventSystems;

public class PauseStateUIHandler : MonoBehaviour {
	private EventSystem eventSystem;
	private GameObject firstSelectedGameObject;
	private void Awake() {
		eventSystem = GetComponentInChildren<EventSystem>();
		firstSelectedGameObject = eventSystem.firstSelectedGameObject;
	}
	private void Start() {
		GameStateManager.Instance.OnPausedStateEnter += () => {
			this.gameObject.SetActive(true);
			eventSystem.SetSelectedGameObject(null);
			eventSystem.SetSelectedGameObject(firstSelectedGameObject);
		};
		GameStateManager.Instance.OnPausedStateExit += () => {
			this.gameObject.SetActive(false);
		};
		this.gameObject.SetActive(false);
	}
}
