﻿using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.Rendering.Universal;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Camera))]
public class OverlayUI : MonoBehaviour {
	private void Awake() {
		string uiSceneName = "UI";
		if (!SceneManager.GetSceneByName(uiSceneName).isLoaded) {
			StartCoroutine(LoadUI(uiSceneName));
		}
	}
	private void Start() {
		Overlay();
	}
	private void Overlay() {
		Camera myCam = GetComponent<Camera>();
		var myCamData = myCam.GetUniversalAdditionalCameraData();
		foreach (Camera cam in GameObject.FindObjectsOfType<Camera>().Where(cam => cam != myCam).Where(cam => cam.GetUniversalAdditionalCameraData().renderType == CameraRenderType.Overlay)) {
			myCamData.cameraStack.Add(cam);
		}
	}

	private IEnumerator LoadUI(string uiSceneName) {
		AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(uiSceneName, LoadSceneMode.Additive);

		while (!asyncLoad.isDone) {
			yield return null;
		}

		Overlay();
	}
}
