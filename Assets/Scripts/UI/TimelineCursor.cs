﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class TimelineCursor : MonoBehaviour {
	[SerializeField]
	private Image timelineImage;

	private RectTransform rt;

	private float length = 15f;
	private float speed = 0.0f;

	private Vector2 originalPosition;
	private void Awake() {
		rt = GetComponent<RectTransform>();

		Image cursorImage = GetComponent<Image>();
		speed = timelineImage.GetComponent<RectTransform>().rect.width / length;
		GameStateManager.Instance.OnExecutionStateEnter += () => {
			cursorImage.enabled = true;
			originalPosition = rt.anchoredPosition;
		};
		GameStateManager.Instance.OnPlanningStateEnter += () => {
			cursorImage.enabled = false;
			rt.anchoredPosition = originalPosition;
		};
	}

	private void Update() {
		if (GameStateManager.Instance.CurrentGameState.State == GameStateManager.State.Executing) {
			rt.anchoredPosition += Vector2.right * speed * Time.deltaTime;
		}
	}
}
