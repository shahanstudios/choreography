﻿using System;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[RequireComponent(typeof(TrackPopulator))]
[RequireComponent(typeof(RectTransform))]
[RequireComponent(typeof(LineRenderer))]
public class TrackUI : MonoBehaviour {
	[SerializeField] private TextMeshProUGUI maxValueLabel = null;
	[SerializeField] private TextMeshProUGUI minValueLabel = null;
	[SerializeField] private float resolution = 0.1f;

	private LineRenderer lineRenderer;
	private Track track;

	public Func<float, float> xTrackPosToXRectPosNormalizeFunc;
	public Func<float, float> yTrackPosToYRectPosNormalizeFunc;

	public Func<float, float> xRectPosToXTrackPosNormalizeFunc;
	public Func<float, float> yRectPosToYTrackPosNormalizeFunc;

	private Dictionary<Track.TrackPoint, GameObject> trackPointImageMap;

	public Track Track => track;

	private void Start() {
		trackPointImageMap = new Dictionary<Track.TrackPoint, GameObject>();
		track = GetComponent<TrackPopulator>().GetTrack();

		lineRenderer = GetComponent<LineRenderer>();

		maxValueLabel.text = track.MaxValue.ToString();
		minValueLabel.text = track.MinValue.ToString();

		RectTransform rectTransform = GetComponent<RectTransform>();
		Rect trackRect = rectTransform.rect;

		Func<float, Vector2, Vector2, float> posNormalizeFunc = (pos, source, dest) => {
			float percentage = (pos - source.x) / (source.y - source.x);
			float destVal = dest.x + (dest.y * percentage);	
			return destVal;
		};
		xTrackPosToXRectPosNormalizeFunc = (xTrackPos) => {
			return posNormalizeFunc(xTrackPos, new Vector2(0, track.Length), new Vector2(trackRect.xMin, trackRect.width));
		};
		yTrackPosToYRectPosNormalizeFunc = (yTrackPos) => {
			return posNormalizeFunc(yTrackPos, new Vector2(track.MinValue, track.MaxValue), new Vector2(trackRect.yMin, trackRect.height));
		};

		xRectPosToXTrackPosNormalizeFunc = (xRectPos) => {
			float normalized = posNormalizeFunc(xRectPos, new Vector2(trackRect.xMin, trackRect.width), new Vector2(0, track.Length));
			return track.GetClosestPointOnGrid(Vector2.right * normalized).x;
		};
		yRectPosToYTrackPosNormalizeFunc = (yRectPos) => {
			float normalized = posNormalizeFunc(yRectPos, new Vector2(trackRect.yMin, trackRect.height), new Vector2(track.MinValue, track.MaxValue - track.MinValue));
			return track.GetClosestPointOnGrid(Vector2.up * normalized).y;
		};

		foreach (Track.TrackPoint point in track.TrackPoints) {
			DisplayTrackPoint(point);
		}

		track.OnTrackPointAdditionEvent += (tp) => {
			DisplayTrackPoint(tp);
			UpdateLineRenderer();
		};
		track.OnTrackPointEditedEvent += (tp) => UpdateLineRenderer();
		track.OnTrackPointRemovedEvent += (tp) => {
			RemoveTrackPoint(tp);
			UpdateLineRenderer();
		};
	}

	public void AddTrackPoint(Vector2 rectLocalPoint) {
		Vector2 newTrackPoint = new Vector2(xRectPosToXTrackPosNormalizeFunc(rectLocalPoint.x), yRectPosToYTrackPosNormalizeFunc(rectLocalPoint.y));
		track.AddPoint(new Track.TrackPoint(newTrackPoint));
	}

	private void RemoveTrackPoint(Track.TrackPoint tp) {
		GameObject.Destroy(trackPointImageMap[tp]);
		trackPointImageMap.Remove(tp);
	}

	private void DisplayTrackPoint(Track.TrackPoint tp) {
		int pointIndex = track.TrackPoints.IndexOf(tp);

		Vector2 point = tp.Location;
		Vector2 pointPos = new Vector2(xTrackPosToXRectPosNormalizeFunc(point.x), yTrackPosToYRectPosNormalizeFunc(point.y));

		GameObject trackPoint = new GameObject();
		trackPoint.name = $"Point {pointIndex}";
		TrackPointUIHandler tph = trackPoint.AddComponent<TrackPointUIHandler>();
		tph.trackPoint = tp;
		tph.trackUI = this;
		trackPoint.transform.SetParent(this.transform, false);
		trackPoint.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(pointPos.x, pointPos.y, -2f);

		trackPointImageMap.Add(tp, trackPoint);

		UpdateLineRenderer();
	}

	private void UpdateLineRenderer() {
		int count = Mathf.CeilToInt(track.Length / resolution);
		lineRenderer.positionCount = count;
		for (int i = 0; i < count; i++) {
			float evalX = 0 + (resolution * i);
			Vector2 evalPoint = track.TrackPath.Evaluate(evalX);
			Vector2 pointPos = new Vector2(xTrackPosToXRectPosNormalizeFunc(evalPoint.x), yTrackPosToYRectPosNormalizeFunc(evalPoint.y));
			Vector3 p = new Vector3(pointPos.x, pointPos.y, -1f);
			lineRenderer.SetPosition(i, p);
		}
	}
}
