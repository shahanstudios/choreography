﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class TrackPointUIHandler : MonoBehaviour {
	public Track.TrackPoint trackPoint;
	public TrackUI trackUI;
	private RectTransform trackTransform;
	private RectTransform pointTransform;
	private float size = 20f;
	private Vector2 lockFactor;
	public void Start() {
		trackTransform = trackUI.GetComponent<RectTransform>();
		pointTransform = GetComponent<RectTransform>();
		pointTransform.sizeDelta = new Vector2(size, size);
		pointTransform.anchorMin = pointTransform.anchorMax = new Vector2(0, 0);

		GetComponent<Image>().color = trackPoint.IsEditable ? Color.white : Color.gray;

		if (!trackPoint.IsEditable) {
			return;
		}

		lockFactor = Vector2.one;
		switch (trackPoint.Lock) {
			case Track.TrackPoint.LockDirection.Horizontal:
				lockFactor = Vector2.up;
				break;
			case Track.TrackPoint.LockDirection.Vertical:
				lockFactor = Vector2.right;
				break;
			case Track.TrackPoint.LockDirection.All:
				lockFactor = Vector2.zero;
				break;
		}
	}

	public void RemovePoint() {
		if (!trackPoint.IsEditable || !trackPoint.IsDeletable) {
			return;
		}
		trackPoint.NotifyNeedsRemoved();
	}	

	public void OnMove(Vector2 change) {
		Rect trackRect = trackTransform.rect;

		Vector2 lockedDelta = change * lockFactor;
		Vector2 newPosition = pointTransform.anchoredPosition + lockedDelta;
		newPosition = new Vector2(
				Mathf.Clamp(newPosition.x, trackRect.xMin, trackRect.xMax),
				Mathf.Clamp(newPosition.y, trackRect.yMin, trackRect.yMax));

		Vector2 newTPLoc = new Vector2(trackUI.xRectPosToXTrackPosNormalizeFunc(newPosition.x), trackUI.yRectPosToYTrackPosNormalizeFunc(newPosition.y));

		Vector2 delta = newTPLoc - trackPoint.Location;
		trackPoint.Move(ref delta);

		if (delta.x == 0) {
			newPosition = new Vector2(pointTransform.anchoredPosition.x, newPosition.y);
		}
		pointTransform.anchoredPosition = newPosition;
	}
}
