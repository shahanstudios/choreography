// GENERATED AUTOMATICALLY FROM 'Assets/TimelineControls.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @TimelineControls : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @TimelineControls()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""TimelineControls"",
    ""maps"": [
        {
            ""name"": ""GameState"",
            ""id"": ""eaafd94b-adf3-4aa9-b23f-71865e267c36"",
            ""actions"": [
                {
                    ""name"": ""ToggleExecutionState"",
                    ""type"": ""Button"",
                    ""id"": ""84ae939d-4f0d-450b-86ea-afae44b17077"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Pause"",
                    ""type"": ""Button"",
                    ""id"": ""0183bb43-8b57-4efe-bcca-30fabb764c6c"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""7a10566d-7032-449e-a6e7-f65e04aee8ef"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""ToggleExecutionState"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""559aaaec-0b28-4020-97c2-360a46869341"",
                    ""path"": ""<Gamepad>/buttonWest"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""ToggleExecutionState"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e6e9ce16-1759-4bd0-bc2f-e355b831a840"",
                    ""path"": ""<Keyboard>/escape"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""Pause"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""f8da29f1-2e88-4f15-815c-f94effdbe64f"",
                    ""path"": ""<Gamepad>/select"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Pause"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""Track"",
            ""id"": ""aa6d6608-261c-4521-8317-1648488c7ae0"",
            ""actions"": [
                {
                    ""name"": ""TrackInteract"",
                    ""type"": ""Button"",
                    ""id"": ""2c134a4c-368f-46fc-8659-f1702c705c8c"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""RemovePoint"",
                    ""type"": ""Button"",
                    ""id"": ""9d1eda0a-c5cb-4e8b-b9ee-f92f620a5ab6"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""MoveCursor"",
                    ""type"": ""Value"",
                    ""id"": ""4828bc8f-dd6f-44e1-89b5-82f5f71ecc0a"",
                    ""expectedControlType"": ""Stick"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""MoveMouse"",
                    ""type"": ""Value"",
                    ""id"": ""58ef4a17-5d87-4cec-b440-c8479548e3f5"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""PanCamera"",
                    ""type"": ""Value"",
                    ""id"": ""c571e9ef-146a-4684-8b97-9f21f6f33169"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ResetCameraPosition"",
                    ""type"": ""Button"",
                    ""id"": ""598f0e8b-e791-4433-a162-7fc170bae893"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""SelectTrack"",
                    ""type"": ""Value"",
                    ""id"": ""e73d90a2-7efb-4547-8d38-9d19db4a0543"",
                    ""expectedControlType"": ""Axis"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""4e58324a-e1a8-460d-aa72-fadf55864e2c"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""TrackInteract"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b8037fac-a4c4-4e12-bb65-9ab99f10cfbc"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""TrackInteract"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a200fd6f-97ee-446a-abec-71591565d080"",
                    ""path"": ""<Keyboard>/enter"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""TrackInteract"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""98ff8db3-6798-4f4c-a926-d282623ae661"",
                    ""path"": ""<Gamepad>/leftStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""MoveCursor"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""Arrows"",
                    ""id"": ""a4e3ce3a-4a9f-4c41-bd1c-10c1f1ba25ae"",
                    ""path"": ""2DVector(mode=2)"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MoveCursor"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""7c91f775-5652-4382-ac4a-fdae79120b39"",
                    ""path"": ""<Keyboard>/upArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""MoveCursor"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""c90403c4-1214-43d2-8756-f0901b56fee4"",
                    ""path"": ""<Keyboard>/downArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""MoveCursor"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""0a402a66-d587-4df2-a216-7c3c54142de4"",
                    ""path"": ""<Keyboard>/leftArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""MoveCursor"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""c5d9c8f3-d436-4245-8920-ad2a8d8cd029"",
                    ""path"": ""<Keyboard>/rightArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""MoveCursor"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""WASD"",
                    ""id"": ""9f1bee2a-a3e1-4e28-a132-1d3ce8a6c68b"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MoveCursor"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""514e3026-3bfe-407c-850e-8526e3e1f504"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""MoveCursor"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""6f2103fe-0ac7-4aa1-b5b3-e3fe747847e7"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""MoveCursor"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""478bf614-6367-4ee0-83b8-ed5e32287731"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""MoveCursor"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""0b783812-41b1-4513-96bc-93ab3f763da7"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""MoveCursor"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""8fa97395-09b1-4123-8bea-7819c127fe8c"",
                    ""path"": ""<Mouse>/position"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""MoveMouse"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""4e2aa720-95d8-4f8a-aa27-b824ea7703b2"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""RemovePoint"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ed572dac-2551-46df-9088-227ff67476dd"",
                    ""path"": ""<Mouse>/rightButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""RemovePoint"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""Button With One Modifier"",
                    ""id"": ""57a75b00-272f-438a-977e-7d9c8e5df3dc"",
                    ""path"": ""ButtonWithOneModifier"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""RemovePoint"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""modifier"",
                    ""id"": ""cd10a8f4-1d99-49af-885c-34fe3907c5f6"",
                    ""path"": ""<Keyboard>/shift"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""RemovePoint"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""button"",
                    ""id"": ""e983da6e-b741-4f7c-be55-fb23bb024310"",
                    ""path"": ""<Keyboard>/enter"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""RemovePoint"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""394967a3-6acf-41ca-bf16-46a37f9dfeb4"",
                    ""path"": ""<Gamepad>/rightStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""PanCamera"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""WASD"",
                    ""id"": ""9ec58fa2-6a92-402e-b7d1-5803239e5d1e"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PanCamera"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""354d563a-1b57-41a4-a06c-ea7c21b9ca95"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""PanCamera"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""b3b5f613-72e0-4edf-a478-220c72456489"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""PanCamera"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""d76bffbc-0df8-43a4-8ab5-4d2e6eece898"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""PanCamera"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""9a490b0a-22c3-4eb2-bdab-d3b8b2e16d0d"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""PanCamera"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Arrows"",
                    ""id"": ""c901fc1c-7dcc-4672-93b1-05131a873607"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PanCamera"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""03e37c65-e54b-402d-a2f6-40521042e937"",
                    ""path"": ""<Keyboard>/upArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""PanCamera"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""b944093a-b1f4-4959-be5d-ea3bf6041674"",
                    ""path"": ""<Keyboard>/downArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""PanCamera"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""c8717aa8-01bc-4755-8860-3a31e894cd2f"",
                    ""path"": ""<Keyboard>/leftArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""PanCamera"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""6de769c9-2a56-4de2-b96e-9a06bd4f4120"",
                    ""path"": ""<Keyboard>/rightArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""PanCamera"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""143c2bc1-05e3-416d-a042-8a77bf43636e"",
                    ""path"": ""<Gamepad>/rightStickPress"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""ResetCameraPosition"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c441878e-0e3b-431f-ae9b-b0c4ae6f2824"",
                    ""path"": ""<Keyboard>/f"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""ResetCameraPosition"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""4faf8e39-2d2d-4bf3-8c69-45bfb2a522c5"",
                    ""path"": ""<Keyboard>/ctrl"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""ResetCameraPosition"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""D-Pad"",
                    ""id"": ""fc2cdcac-99a1-41aa-af53-c4631aa5a475"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""SelectTrack"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""527c06a1-37fc-46d9-9e31-bceca7535214"",
                    ""path"": ""<Gamepad>/dpad/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""SelectTrack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""794479d5-ecc1-44d9-a809-e4bcee4a90d1"",
                    ""path"": ""<Gamepad>/dpad/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""SelectTrack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Keys"",
                    ""id"": ""3c448cd7-19bf-410d-acf6-f90941d88f01"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""SelectTrack"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""6e07bfff-fa2f-49d6-981c-1795c6b450f9"",
                    ""path"": ""<Keyboard>/i"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""SelectTrack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""3f0a7101-4082-40e2-80ff-d53450ca4060"",
                    ""path"": ""<Keyboard>/k"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""SelectTrack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Keys"",
                    ""id"": ""d654116a-c4b7-4d06-9fd7-82b4b93adf17"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""SelectTrack"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""5e144659-a7ab-40fe-b318-3c000e59fd87"",
                    ""path"": ""<Keyboard>/e"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""SelectTrack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""d88d4f91-0783-4351-953f-2c75bb400265"",
                    ""path"": ""<Keyboard>/q"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""SelectTrack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""Gamepad"",
            ""bindingGroup"": ""Gamepad"",
            ""devices"": [
                {
                    ""devicePath"": ""<Gamepad>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        },
        {
            ""name"": ""Keyboard & Mouse"",
            ""bindingGroup"": ""Keyboard & Mouse"",
            ""devices"": [
                {
                    ""devicePath"": ""<Keyboard>"",
                    ""isOptional"": false,
                    ""isOR"": false
                },
                {
                    ""devicePath"": ""<Mouse>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        }
    ]
}");
        // GameState
        m_GameState = asset.FindActionMap("GameState", throwIfNotFound: true);
        m_GameState_ToggleExecutionState = m_GameState.FindAction("ToggleExecutionState", throwIfNotFound: true);
        m_GameState_Pause = m_GameState.FindAction("Pause", throwIfNotFound: true);
        // Track
        m_Track = asset.FindActionMap("Track", throwIfNotFound: true);
        m_Track_TrackInteract = m_Track.FindAction("TrackInteract", throwIfNotFound: true);
        m_Track_RemovePoint = m_Track.FindAction("RemovePoint", throwIfNotFound: true);
        m_Track_MoveCursor = m_Track.FindAction("MoveCursor", throwIfNotFound: true);
        m_Track_MoveMouse = m_Track.FindAction("MoveMouse", throwIfNotFound: true);
        m_Track_PanCamera = m_Track.FindAction("PanCamera", throwIfNotFound: true);
        m_Track_ResetCameraPosition = m_Track.FindAction("ResetCameraPosition", throwIfNotFound: true);
        m_Track_SelectTrack = m_Track.FindAction("SelectTrack", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // GameState
    private readonly InputActionMap m_GameState;
    private IGameStateActions m_GameStateActionsCallbackInterface;
    private readonly InputAction m_GameState_ToggleExecutionState;
    private readonly InputAction m_GameState_Pause;
    public struct GameStateActions
    {
        private @TimelineControls m_Wrapper;
        public GameStateActions(@TimelineControls wrapper) { m_Wrapper = wrapper; }
        public InputAction @ToggleExecutionState => m_Wrapper.m_GameState_ToggleExecutionState;
        public InputAction @Pause => m_Wrapper.m_GameState_Pause;
        public InputActionMap Get() { return m_Wrapper.m_GameState; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(GameStateActions set) { return set.Get(); }
        public void SetCallbacks(IGameStateActions instance)
        {
            if (m_Wrapper.m_GameStateActionsCallbackInterface != null)
            {
                @ToggleExecutionState.started -= m_Wrapper.m_GameStateActionsCallbackInterface.OnToggleExecutionState;
                @ToggleExecutionState.performed -= m_Wrapper.m_GameStateActionsCallbackInterface.OnToggleExecutionState;
                @ToggleExecutionState.canceled -= m_Wrapper.m_GameStateActionsCallbackInterface.OnToggleExecutionState;
                @Pause.started -= m_Wrapper.m_GameStateActionsCallbackInterface.OnPause;
                @Pause.performed -= m_Wrapper.m_GameStateActionsCallbackInterface.OnPause;
                @Pause.canceled -= m_Wrapper.m_GameStateActionsCallbackInterface.OnPause;
            }
            m_Wrapper.m_GameStateActionsCallbackInterface = instance;
            if (instance != null)
            {
                @ToggleExecutionState.started += instance.OnToggleExecutionState;
                @ToggleExecutionState.performed += instance.OnToggleExecutionState;
                @ToggleExecutionState.canceled += instance.OnToggleExecutionState;
                @Pause.started += instance.OnPause;
                @Pause.performed += instance.OnPause;
                @Pause.canceled += instance.OnPause;
            }
        }
    }
    public GameStateActions @GameState => new GameStateActions(this);

    // Track
    private readonly InputActionMap m_Track;
    private ITrackActions m_TrackActionsCallbackInterface;
    private readonly InputAction m_Track_TrackInteract;
    private readonly InputAction m_Track_RemovePoint;
    private readonly InputAction m_Track_MoveCursor;
    private readonly InputAction m_Track_MoveMouse;
    private readonly InputAction m_Track_PanCamera;
    private readonly InputAction m_Track_ResetCameraPosition;
    private readonly InputAction m_Track_SelectTrack;
    public struct TrackActions
    {
        private @TimelineControls m_Wrapper;
        public TrackActions(@TimelineControls wrapper) { m_Wrapper = wrapper; }
        public InputAction @TrackInteract => m_Wrapper.m_Track_TrackInteract;
        public InputAction @RemovePoint => m_Wrapper.m_Track_RemovePoint;
        public InputAction @MoveCursor => m_Wrapper.m_Track_MoveCursor;
        public InputAction @MoveMouse => m_Wrapper.m_Track_MoveMouse;
        public InputAction @PanCamera => m_Wrapper.m_Track_PanCamera;
        public InputAction @ResetCameraPosition => m_Wrapper.m_Track_ResetCameraPosition;
        public InputAction @SelectTrack => m_Wrapper.m_Track_SelectTrack;
        public InputActionMap Get() { return m_Wrapper.m_Track; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(TrackActions set) { return set.Get(); }
        public void SetCallbacks(ITrackActions instance)
        {
            if (m_Wrapper.m_TrackActionsCallbackInterface != null)
            {
                @TrackInteract.started -= m_Wrapper.m_TrackActionsCallbackInterface.OnTrackInteract;
                @TrackInteract.performed -= m_Wrapper.m_TrackActionsCallbackInterface.OnTrackInteract;
                @TrackInteract.canceled -= m_Wrapper.m_TrackActionsCallbackInterface.OnTrackInteract;
                @RemovePoint.started -= m_Wrapper.m_TrackActionsCallbackInterface.OnRemovePoint;
                @RemovePoint.performed -= m_Wrapper.m_TrackActionsCallbackInterface.OnRemovePoint;
                @RemovePoint.canceled -= m_Wrapper.m_TrackActionsCallbackInterface.OnRemovePoint;
                @MoveCursor.started -= m_Wrapper.m_TrackActionsCallbackInterface.OnMoveCursor;
                @MoveCursor.performed -= m_Wrapper.m_TrackActionsCallbackInterface.OnMoveCursor;
                @MoveCursor.canceled -= m_Wrapper.m_TrackActionsCallbackInterface.OnMoveCursor;
                @MoveMouse.started -= m_Wrapper.m_TrackActionsCallbackInterface.OnMoveMouse;
                @MoveMouse.performed -= m_Wrapper.m_TrackActionsCallbackInterface.OnMoveMouse;
                @MoveMouse.canceled -= m_Wrapper.m_TrackActionsCallbackInterface.OnMoveMouse;
                @PanCamera.started -= m_Wrapper.m_TrackActionsCallbackInterface.OnPanCamera;
                @PanCamera.performed -= m_Wrapper.m_TrackActionsCallbackInterface.OnPanCamera;
                @PanCamera.canceled -= m_Wrapper.m_TrackActionsCallbackInterface.OnPanCamera;
                @ResetCameraPosition.started -= m_Wrapper.m_TrackActionsCallbackInterface.OnResetCameraPosition;
                @ResetCameraPosition.performed -= m_Wrapper.m_TrackActionsCallbackInterface.OnResetCameraPosition;
                @ResetCameraPosition.canceled -= m_Wrapper.m_TrackActionsCallbackInterface.OnResetCameraPosition;
                @SelectTrack.started -= m_Wrapper.m_TrackActionsCallbackInterface.OnSelectTrack;
                @SelectTrack.performed -= m_Wrapper.m_TrackActionsCallbackInterface.OnSelectTrack;
                @SelectTrack.canceled -= m_Wrapper.m_TrackActionsCallbackInterface.OnSelectTrack;
            }
            m_Wrapper.m_TrackActionsCallbackInterface = instance;
            if (instance != null)
            {
                @TrackInteract.started += instance.OnTrackInteract;
                @TrackInteract.performed += instance.OnTrackInteract;
                @TrackInteract.canceled += instance.OnTrackInteract;
                @RemovePoint.started += instance.OnRemovePoint;
                @RemovePoint.performed += instance.OnRemovePoint;
                @RemovePoint.canceled += instance.OnRemovePoint;
                @MoveCursor.started += instance.OnMoveCursor;
                @MoveCursor.performed += instance.OnMoveCursor;
                @MoveCursor.canceled += instance.OnMoveCursor;
                @MoveMouse.started += instance.OnMoveMouse;
                @MoveMouse.performed += instance.OnMoveMouse;
                @MoveMouse.canceled += instance.OnMoveMouse;
                @PanCamera.started += instance.OnPanCamera;
                @PanCamera.performed += instance.OnPanCamera;
                @PanCamera.canceled += instance.OnPanCamera;
                @ResetCameraPosition.started += instance.OnResetCameraPosition;
                @ResetCameraPosition.performed += instance.OnResetCameraPosition;
                @ResetCameraPosition.canceled += instance.OnResetCameraPosition;
                @SelectTrack.started += instance.OnSelectTrack;
                @SelectTrack.performed += instance.OnSelectTrack;
                @SelectTrack.canceled += instance.OnSelectTrack;
            }
        }
    }
    public TrackActions @Track => new TrackActions(this);
    private int m_GamepadSchemeIndex = -1;
    public InputControlScheme GamepadScheme
    {
        get
        {
            if (m_GamepadSchemeIndex == -1) m_GamepadSchemeIndex = asset.FindControlSchemeIndex("Gamepad");
            return asset.controlSchemes[m_GamepadSchemeIndex];
        }
    }
    private int m_KeyboardMouseSchemeIndex = -1;
    public InputControlScheme KeyboardMouseScheme
    {
        get
        {
            if (m_KeyboardMouseSchemeIndex == -1) m_KeyboardMouseSchemeIndex = asset.FindControlSchemeIndex("Keyboard & Mouse");
            return asset.controlSchemes[m_KeyboardMouseSchemeIndex];
        }
    }
    public interface IGameStateActions
    {
        void OnToggleExecutionState(InputAction.CallbackContext context);
        void OnPause(InputAction.CallbackContext context);
    }
    public interface ITrackActions
    {
        void OnTrackInteract(InputAction.CallbackContext context);
        void OnRemovePoint(InputAction.CallbackContext context);
        void OnMoveCursor(InputAction.CallbackContext context);
        void OnMoveMouse(InputAction.CallbackContext context);
        void OnPanCamera(InputAction.CallbackContext context);
        void OnResetCameraPosition(InputAction.CallbackContext context);
        void OnSelectTrack(InputAction.CallbackContext context);
    }
}
