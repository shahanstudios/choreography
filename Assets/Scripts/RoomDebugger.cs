﻿using UnityEngine;

public class RoomDebugger : MonoBehaviour {
	public Room room = null;
	public bool showRoomBounds = false;

	private void OnDrawGizmos() {
		if (room == null || !showRoomBounds) {
			return;
		}

		Gizmos.color = Color.green;
		Bounds bounds = room.Bounds;

		Gizmos.DrawLine(new Vector2(bounds.min.x, bounds.min.y), new Vector2(bounds.max.x, bounds.min.y));
		Gizmos.DrawLine(new Vector2(bounds.max.x, bounds.min.y), new Vector2(bounds.max.x, bounds.max.y));
		Gizmos.DrawLine(new Vector2(bounds.min.x, bounds.max.y), new Vector2(bounds.max.x, bounds.max.y));
		Gizmos.DrawLine(new Vector2(bounds.min.x, bounds.max.y), new Vector2(bounds.min.x, bounds.min.y));
	}
}
