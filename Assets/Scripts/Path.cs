﻿using UnityEngine;

public class Path {
	private Track.TrackPoint firstPoint;

	public Path(Track.TrackPoint point) {
		firstPoint = point;
	}

	public virtual void InsertAnchorPoint(Track.TrackPoint anchorPoint) {
		var prevPoint = FindPointBeforeGivenX(anchorPoint.Location.x);

		anchorPoint.SetPreviousPoint(prevPoint);
		anchorPoint.SetNextPoint(prevPoint.NextPoint);
		prevPoint?.NextPoint?.SetPreviousPoint(anchorPoint);
		prevPoint?.SetNextPoint(anchorPoint);
	}
	public virtual void RemoveAnchorPoint(Track.TrackPoint anchorPoint) {
		anchorPoint.PrevPoint?.SetNextPoint(anchorPoint.NextPoint);
		anchorPoint.NextPoint?.SetPreviousPoint(anchorPoint.PrevPoint);
	}
	public virtual Vector2 Evaluate(float dist) {
		var prevPoint = FindPointBeforeGivenX(dist);
		var nextPoint = prevPoint.NextPoint;
		Vector2 v0 = prevPoint.Location;

		if (nextPoint == null) {
			return v0;
		}

		Vector2 v1 = prevPoint.NextPoint.Location;

		float t = (dist - v0.x) / (v1.x - v0.x);
		float interpY = Interpolation.Interpolate(v0.y, v1.y, t, Interpolation.CrossFade(prevPoint.StartInterpFunc, nextPoint.StopInterpFunc));
		return new Vector2(dist, interpY);
	}
	public Track.TrackPoint FindPointBeforeGivenX(float x) {
		Track.TrackPoint point = firstPoint;
		while (point.NextPoint?.Location.x <= x) {
			point = point.NextPoint;
		}

		return point;
	}

	protected void PrintPoints() {
		var point = firstPoint;
		string pts = $"{point.Location}, ";
		while (point.NextPoint != null) {
			pts += $"{point.NextPoint.Location}, ";
			point = point.NextPoint;
		}
		Debug.Log(pts);
	}
}
