﻿using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;

public class GameStateManager : MonoBehaviour {
	private static GameStateManager instance;
	public static GameStateManager Instance {
		get {
			if (instance == null) {
				GameObject gameStateManager = new GameObject();
				gameStateManager.name = "_GameStateManager";
				instance = gameStateManager.AddComponent<GameStateManager>();
			}
			return instance;
		}
	}
	public static GameStateManager RawInstance => instance;

	private TimelineControls timelineControls;

	private GameState gameState;
	public GameState CurrentGameState => gameState;
	private StateMachine<GameState> stateMachine;

	public event System.Action OnPlanningStateEnter;
	public event System.Action OnExecutionStateEnter;
	public event System.Action OnPausedStateEnter;
	public event System.Action OnPausedStateExit;

	private State<GameState> planning;
	private State<GameState> executing;

	private int framesSincePaused = 0;

	private void Awake() {
		if (instance != null && instance != this) {
			Destroy(this.gameObject);
		}
		else {
			instance = this;
			Time.timeScale = 1.0f;
			DontDestroyOnLoad(this.gameObject);
		}

		timelineControls = new TimelineControls();
		
		this.gameState = new GameState();
		planning = new State<GameState>("Planning");
		planning.OnStateEnterEvent += (gs) => {
			gameState.SetState(State.Planning);
			if (gs?.PrevState != State.Paused) {
				OnPlanningStateEnter?.Invoke();
			}
		};
		executing = new State<GameState>("Executing");
		executing.OnStateEnterEvent += (gs) => {
			gameState.SetState(State.Executing);
			if (gs?.PrevState != State.Paused) {
				OnExecutionStateEnter?.Invoke();
			}
		};
		var paused = new State<GameState>("Paused");
		
		System.Action<InputAction.CallbackContext> resumeAction = (InputAction.CallbackContext context) => Resume();

		paused.OnStateEnterEvent += (_) => {
			gameState.SetState(State.Paused);
			Time.timeScale = 0.0f;
			OnPausedStateEnter?.Invoke();
			timelineControls.GameState.Pause.performed += resumeAction;
			framesSincePaused = 0;
		};
		paused.OnStateExitEvent += (_) => {
			Time.timeScale = 1.0f;
			OnPausedStateExit?.Invoke();
			timelineControls.GameState.Pause.performed -= resumeAction;
		};

		Condition<GameState> executeToggleCondition = new System.Predicate<GameState>(state =>
		{
			return timelineControls.GameState.ToggleExecutionState.triggered;
		});
		Condition<GameState> pauseToggleCondition = new System.Predicate<GameState>(state =>
		{
			return timelineControls.GameState.Pause.triggered && framesSincePaused > 0;
		});

		planning.AddTransition(new Transition<GameState>(executeToggleCondition, executing));
		executing.AddTransition(new Transition<GameState>(executeToggleCondition, planning));
		planning.AddTransition(new Transition<GameState>(pauseToggleCondition, paused));
		executing.AddTransition(new Transition<GameState>(pauseToggleCondition, paused));

		stateMachine = new StateMachine<GameState>(this.gameState, planning, executing);

		RoomLoader.Instance.OnLoadRoomEvent += (room) => {
			SetPlanning();
		};
	}

	public void SetPlanning() {
		stateMachine.TransitionToState(planning);
	}
	public void SetExecuting() {
		stateMachine.TransitionToState(executing);
	}
	public void Resume() {
		if (CurrentGameState.State == State.Paused) {
			State prev = CurrentGameState.PrevState;
			if (prev == State.Planning) {
				SetPlanning();
			}
			else if (prev == State.Executing) {
				SetExecuting();
			}
		}
	}

	private void Update() {
		stateMachine.Update();
		if (CurrentGameState.State != State.Paused) {
			framesSincePaused++;
		}
	}

	private void OnEnable() {
		timelineControls?.Enable();
	}
	private void OnDisable() {
		timelineControls?.Disable();
	}

	public class GameState {
		public State State { get; private set; }		
		public State PrevState { get; private set; }

		public GameState() {
			State = State.Planning;
			PrevState = State.Planning;
		}
		public void SetState(State state) {
			PrevState = State;
			State = state;
		}
	}

	public enum State {
		Planning,
		Executing,
		Paused
	}
}
