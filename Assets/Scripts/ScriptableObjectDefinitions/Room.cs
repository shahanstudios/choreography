﻿using UnityEngine;

[CreateAssetMenu(menuName = "Choreography/Room")]
public class Room : ScriptableObject {
	[SerializeField]
	private string sceneName;
	[SerializeField]
	private Vector2 defaultPlayerLocation;
	[SerializeField]
	private Exit[] exits;
	[SerializeField]
	private Bounds bounds;

	public string SceneName => sceneName;
	public Vector2 DefaultPlayerLocation => defaultPlayerLocation;
	public Bounds Bounds => bounds;

	public Exit GetExit(int index) {
		return exits[index];
	}

	[System.Serializable]
	public class Exit {
		public Room room = null;
		public Vector2 position = Vector2.zero;
	}
}
