﻿using System.Collections.Generic;
using UnityEngine;

public class JumpTrack : Track {
	private Dictionary<Track.TrackPoint, (Track.TrackPoint, Track.TrackPoint)> anchorToControlPointMap;
	private float jumpDuration;

	public JumpTrack(float minValue, float maxValue, float startingValue, float length, float yResolution, float jumpDuration) : this(minValue, maxValue, startingValue, startingValue, length, yResolution, jumpDuration) { }
	public JumpTrack(float minValue, float maxValue, float startingValue, float endingValue, float length, float yResolution, float jumpDuration) :
	 base(minValue, maxValue, startingValue, endingValue, length, new Vector2(jumpDuration, yResolution)) {
		 this.jumpDuration = jumpDuration;

		DefaultTrackStartInterpFunc = Interpolation.Linear;
		DefaultTrackStopInterpFunc = Interpolation.Linear;

		anchorToControlPointMap = new Dictionary<Track.TrackPoint, (Track.TrackPoint, Track.TrackPoint)>();
	 }

	public override void AddPoint(TrackPoint trackPoint, bool fireAdditionEvent = true, bool insertToPath = true) {
		if (this.TrackPoints?.Count >= 2) {
			var basePoint = new TrackPoint(new Vector2(trackPoint.Location.x, this.minValue), Track.TrackPoint.LockDirection.All, false);
			var tailPoint = new TrackPoint(new Vector2(trackPoint.Location.x + this.jumpDuration, this.minValue), Track.TrackPoint.LockDirection.All, false);
			base.AddPoint(basePoint, false);
			base.AddPoint(tailPoint, false);

			anchorToControlPointMap.Add(trackPoint, (basePoint, tailPoint));

			trackPoint.OnPreEdit += (Track.TrackPoint tp, ref Vector2 delta) => {
				float dirX = Mathf.Sign(delta.x);
				float epsilon = 0.1f;
				if (dirX == -1) {
					float dist = (basePoint.Location.x + delta.x) - basePoint.PrevPoint.Location.x;
					if (Mathf.Abs(dist) > epsilon) {
						basePoint.Move(Vector2.right * delta.x);
					}
					else {
						delta = new Vector2(0, delta.y);
					}
				}
				else if (dirX == 1) {
					float dist = (tailPoint.Location.x + delta.x) - tailPoint.NextPoint.Location.x;
					if (Mathf.Abs(dist) > epsilon) {
						tailPoint.Move(Vector2.right * delta.x);
					}
					else {
						delta = new Vector2(0, delta.y);
					}
				}
			};
			trackPoint.OnEdit += (tp, delta) => {
				float dirX = Mathf.Sign(delta.x);
				if (dirX == -1) {
					tailPoint.SetLocation(new Vector2(tp.Location.x + this.jumpDuration, this.minValue));
				}
				else if (dirX == 1) {
					basePoint.SetLocation(new Vector2(tp.Location.x, this.minValue));
				}
			};
		}
		base.AddPoint(trackPoint, fireAdditionEvent, insertToPath);
	}

	protected override void RemovePoint(TrackPoint trackPoint, bool fireRemovalEvent = true) {
		if (anchorToControlPointMap.ContainsKey(trackPoint)) {
			var partnerPoints = anchorToControlPointMap[trackPoint];
			base.RemovePoint(partnerPoints.Item1, false);
			base.RemovePoint(partnerPoints.Item2, false);
			anchorToControlPointMap.Remove(trackPoint);
		}
		base.RemovePoint(trackPoint, fireRemovalEvent);
	}
}
