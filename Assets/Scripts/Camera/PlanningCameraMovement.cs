﻿using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(Transform))]
public class PlanningCameraMovement : MonoBehaviour {
	[SerializeField] private Transform playerTransform;
	[SerializeField] private float movementSpeed = 10.0f;

	private new Transform transform;
	private TimelineControls timelineControls;

	private InputAction panCameraAction;

	private void Awake() {
		transform = GetComponent<Transform>();	
		timelineControls = new TimelineControls();

		panCameraAction = timelineControls.Track.PanCamera;

		GameStateManager.Instance.OnExecutionStateEnter += () => {
			this.enabled = false;
		};
		GameStateManager.Instance.OnPlanningStateEnter += () => {
			this.enabled = true;
		};
	}

	private void OnEnable() {
		timelineControls.Enable();
		timelineControls.Track.ResetCameraPosition.performed += ResetCameraPosition;
	}
	private void OnDisable() {
		timelineControls.Disable();
		timelineControls.Track.ResetCameraPosition.performed -= ResetCameraPosition;
	}

	private void Update() {
		PanCamera(panCameraAction.ReadValue<Vector2>());
	}

	private void PanCamera(Vector2 dir) {
		transform.Translate(dir * movementSpeed * Time.deltaTime);
	}
	private void ResetCameraPosition(InputAction.CallbackContext context) {
		transform.position = new Vector3(playerTransform.position.x, playerTransform.position.y, transform.position.z);		
	}
}
