﻿using UnityEngine;

public class ExecutingCameraMovement : MonoBehaviour {
	[SerializeField] private Transform playerTransform;

	private new Transform transform;
	private Vector3 initialOffsetToPlayer;

	private void Awake() {
		transform = GetComponent<Transform>();	

		initialOffsetToPlayer = playerTransform.position - transform.position;

		this.enabled = false;
		GameStateManager.Instance.OnExecutionStateEnter += () => {
			this.enabled = true;
		};
		GameStateManager.Instance.OnPlanningStateEnter += () => {
			this.enabled = false;
		};
	}

	private void Update() {
		transform.position = playerTransform.position - initialOffsetToPlayer;
	}
}
