﻿using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CameraBounds : MonoBehaviour {
	private BoxCollider2D roomCollider;

	private new Camera camera;
	private BoxCollider2D cameraCollider;

	private Bounds? cameraMovementBounds;

	private void Awake() {
		camera = GetComponent<Camera>();

		Rect pr = camera.pixelRect;
		Vector2 minPoint = camera.ScreenToWorldPoint(pr.min);
		Vector2 maxPoint = camera.ScreenToWorldPoint(pr.max);
		cameraCollider = this.gameObject.AddComponent<BoxCollider2D>();
		cameraCollider.size = new Vector2(maxPoint.x - minPoint.x, maxPoint.y - minPoint.y);
		cameraCollider.isTrigger = true;

		RoomLoader.Instance.OnLoadRoomEvent += (room) => {
			SetRoomBounds(room.Bounds);
		};
	}

	public void SetRoomBounds(Bounds roomBounds) {
		Bounds camBounds = cameraCollider.bounds;
		Vector2 boundsSize = new Vector2(Mathf.Max(roomBounds.size.x - camBounds.size.x, 0), Mathf.Max(roomBounds.size.y - camBounds.size.y, 0));
		Vector2 boundsCenter = new Vector2(boundsSize.x > 0 ? roomBounds.center.x : camBounds.center.x, boundsSize.y > 0 ? roomBounds.center.y : camBounds.center.y);
		cameraMovementBounds = new Bounds(boundsCenter, boundsSize);
	}

	private void LateUpdate() {
		if (cameraMovementBounds.HasValue) {
			Bounds cameraBounds = cameraMovementBounds.GetValueOrDefault();
			transform.position = 
				new Vector3(
					Mathf.Clamp(transform.position.x, cameraBounds.min.x, cameraBounds.max.x),
					Mathf.Clamp(transform.position.y, cameraBounds.min.y, cameraBounds.max.y),
					transform.position.z
				);
		}
	}
}
