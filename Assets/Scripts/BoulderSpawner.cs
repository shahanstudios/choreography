﻿using UnityEngine;

public class BoulderSpawner : MonoBehaviour {
	[SerializeField]
	private GameObject boulderPrefab = null;
	[SerializeField]
	private float boulderSpeed = 5.0f;
	[SerializeField]
	private float rateOfSpawn = 3.0f;
	[SerializeField]
	private float spawnTimingOffset = 0.0f;
	[SerializeField]
	private bool doSpawnInstantly = false;

	private float currentSpawnTime;
	private System.Action onPlanningReaction;
	
	private float defaultSpawnTime => doSpawnInstantly ? rateOfSpawn - spawnTimingOffset : -spawnTimingOffset;

	protected void Awake() {
		this.onPlanningReaction = () => {
			currentSpawnTime = defaultSpawnTime;
			foreach (Transform child in transform) {
				GameObject.Destroy(child.gameObject);
			}
		};
		currentSpawnTime = defaultSpawnTime;
		GameStateManager.Instance.OnPlanningStateEnter += this.onPlanningReaction;
	}

	protected void Update() {
		if (GameStateManager.Instance.CurrentGameState.State == GameStateManager.State.Executing) {
			currentSpawnTime += Time.deltaTime;
			if (currentSpawnTime >= rateOfSpawn) {
				SpawnBoulder();
				currentSpawnTime = 0;
			}
		}
	}

	private void SpawnBoulder() {
		GameObject boulder = GameObject.Instantiate(boulderPrefab, transform.position, Quaternion.identity);
		boulder.name = "Boulder";
		boulder.transform.parent = transform;
		MoveWithConstantVelocity movement = boulder.AddComponent<MoveWithConstantVelocity>();
		movement.Velocity = Vector3.down * boulderSpeed;
	}

	public void OnDestroy() {
		if (GameStateManager.RawInstance) {
			GameStateManager.RawInstance.OnPlanningStateEnter -= this.onPlanningReaction;
		}
	}
}
