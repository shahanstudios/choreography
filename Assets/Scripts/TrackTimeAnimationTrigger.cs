﻿using UnityEngine;

[RequireComponent(typeof(Animator))]
public class TrackTimeAnimationTrigger : TrackTimeTrigger {
	private Animator animator;

	[SerializeField]
	private TimedAnimationTrigger[] animationTriggers;

	private new void Awake() {
		this.animator = GetComponent<Animator>();
		base.Awake();
		triggerEvents = new TrackTriggerEvent[animationTriggers.Length];
		for (int i = 0; i < animationTriggers.Length; i++) {
			TimedAnimationTrigger animTrigger = animationTriggers[i];
			triggerEvents[i] = new TrackAnimationTriggerEvent(animTrigger.timeToTrigger, this.animator, animTrigger.triggerName);
		}
	}

	public class TrackAnimationTriggerEvent : TrackTimeTrigger.TrackTriggerEvent {
		private Animator animator;
		private string triggerName;

		public TrackAnimationTriggerEvent(float timeToTrigger, Animator animator, string triggerName)
			: base(timeToTrigger, () => {
				animator.SetBool(triggerName, true);
			}) {
				this.animator = animator;
				this.triggerName = triggerName;
			}

		protected override void ResetTrigger() {
			animator?.SetBool(triggerName, false);
		}
	}

	[System.Serializable]
	public struct TimedAnimationTrigger {
		public float timeToTrigger;
		public string triggerName;
	}
}
