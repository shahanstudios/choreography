﻿using UnityEngine;

public class MoveWithConstantVelocity : MonoBehaviour {
	public Vector3 Velocity;

	private void FixedUpdate() {
		transform.Translate(Velocity * Time.deltaTime);
	}
}
