﻿using System;
using UnityEngine;

public class Interpolation {
	public static float Interpolate(float a, float b, float percent, Func<float, float> interpFunc) {
		return (a + interpFunc(percent) * (b - a));
	}

	public static float Linear(float t) {
		return t;
	}

	public static float SmoothStart2(float t) {
		return t * t;
	}
	public static float SmoothStart3(float t) {
		return t * t * t;
	}
	public static float SmoothStart4(float t) {
		return t * t * t * t;
	}

	public static float SmoothStop2(float t) {
		return 1 - (1 - t) * (1 - t);
	}
	public static float SmoothStop3(float t) {
		return 1 - (1 - t) * (1 - t) * (1 - t);
	}
	public static float SmoothStop4(float t) {
		return 1 - (1 - t) * (1 - t) * (1 - t) * (1 - t);
	}

	public static float Mix(Func<float, float> a, Func<float, float> b, float blend, float percent) {
		return a(percent) * (1 - blend) + blend * b(percent);	
	}
	public static Func<float, float> CrossFade(Func<float, float> a, Func<float, float> b) {
		return (t) => Mix(a, b, t, t);
	}
	public static float CrossFade(Func<float, float> a, Func<float, float> b, float percent) {
		return Mix(a, b, percent, percent);
	}

	public static float SmoothStep2(float t) {
		return CrossFade(SmoothStart2, SmoothStop2, t);
	}
	public static float SmoothStep3(float t) {
		return CrossFade(SmoothStart3, SmoothStop3, t);
	}
	public static float SmoothStep4(float t) {
		return CrossFade(SmoothStart4, SmoothStop4, t);
	}

	public static float Floor(float t) {
		return Mathf.Floor(t);
	}
	public static float Ceil(float t) {
		return Mathf.Ceil(t);
	}

	// Cubic (3rd) Beizer through A, B, C, D where A (start) and D (end) are assumed to be 0 and 1
	public static float NormalizedBezier3(float B, float C, float t) {
		float s = 1 - t;
		float t2 = t * t;
		float s2 = s * s;
		float t3 = t2 * t;
		return (3 * B * s2 * t) + (3 * C * s * t2) + t3;
	}
}
