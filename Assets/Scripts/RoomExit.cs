﻿using UnityEngine;

public class RoomExit : MonoBehaviour {
	[SerializeField]
	private Room room = null;
	[SerializeField]
	private int exitIndex = 0;

	private Room.Exit exit;

	private System.Action<Room> newRoomLoadFunc;
	private void Awake() {
		exit = room.GetExit(exitIndex);
	}
	private void OnTriggerEnter2D(Collider2D collider) {
		if (collider.CompareTag("Player")) {
			collider.enabled = false;
			newRoomLoadFunc = (room) => {
				collider.transform.position = exit.position;
				collider.enabled = true;
				RoomLoader.Instance.OnLoadRoomEvent -= newRoomLoadFunc;
			};
			RoomLoader.Instance.OnLoadRoomEvent += newRoomLoadFunc;

			StartCoroutine(LoadAndUnloadRooms());
		}
	}
	System.Collections.IEnumerator LoadAndUnloadRooms() {
		yield return RoomLoader.Instance.Load(exit.room);
		yield return RoomLoader.Instance.Unload(room);
	}
}
