﻿using UnityEngine;

public class Projectile : MonoBehaviour {
	public void OnTriggerEnter2D(Collider2D collider) {
		if (collider.CompareTag("Player")) {
			GameStateManager.Instance.SetPlanning();
		}
	}
}
