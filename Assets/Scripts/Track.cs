﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Track {
	protected float minValue;
	protected float maxValue;
	protected float startingValue;
	protected float endingValue;
	protected float length;
	protected bool isEditable;

	protected Vector2 trackResolution;

	public float MinValue => minValue;
	public float MaxValue => maxValue;
	public float Length => length;
	public bool IsEditable => isEditable;

	protected List<TrackPoint> anchorPoints;
	public List<TrackPoint> TrackPoints => anchorPoints;
	protected Path trackPath;
	public Path TrackPath => trackPath;

	public event System.Action<TrackPoint> OnTrackPointAdditionEvent;
	public event System.Action<TrackPoint> OnTrackPointEditedEvent;
	public event System.Action<TrackPoint> OnTrackPointRemovedEvent;

	public Func<float, float> DefaultTrackStartInterpFunc = Interpolation.SmoothStep4;
	public Func<float, float> DefaultTrackStopInterpFunc = Interpolation.SmoothStep4;

	public Track() : this(-1f, 1f, 0f, 30f, new Vector2(0.5f, 0.25f)) { }
	public Track(float minValue, float maxValue, float startingValue, float length, Vector2 resolution) : this(minValue, maxValue, startingValue, startingValue, length, resolution) { }
	public Track(float minValue, float maxValue, float startingValue, float endingValue, float length, Vector2 resolution) {
		this.minValue = minValue;
		this.maxValue = maxValue;
		this.startingValue = startingValue;
		this.endingValue = endingValue;
		this.length = length;
		this.isEditable = true;

		this.trackResolution = resolution;

		OnTrackPointAdditionEvent += (tp) => {
			if (this.anchorPoints == null) {
				this.anchorPoints = new List<Track.TrackPoint>();
			}
			this.anchorPoints.Add(tp);
		};
	}
	public void InitializePath(bool canMoveSecondPoint = true) {
		var firstPoint = new TrackPoint(new Vector2(0, startingValue), TrackPoint.LockDirection.All, false);
		var secondPointLock = canMoveSecondPoint ? TrackPoint.LockDirection.Horizontal : TrackPoint.LockDirection.All;
		var secondPoint = new TrackPoint(new Vector2(this.length, endingValue), secondPointLock, false);

		firstPoint.SetNextPoint(secondPoint);
		secondPoint.SetPreviousPoint(firstPoint);
		this.AddPoint(firstPoint, true, false);
		this.AddPoint(secondPoint, true, false);

		trackPath = new Path(firstPoint);
	}
	public virtual void AddPoint(TrackPoint trackPoint, bool fireAdditionEvent = true, bool insertToPath = true) {
		trackPoint.StopInterpFunc = DefaultTrackStopInterpFunc;
		trackPoint.StartInterpFunc = DefaultTrackStartInterpFunc;
		trackPoint.OnEdit += (tp, d) => OnTrackPointEditedEvent?.Invoke(trackPoint);
		trackPoint.OnNeedsRemoved += (tp) => RemovePoint(tp);

		if (insertToPath) {
			trackPath.InsertAnchorPoint(trackPoint);
		}

		if (fireAdditionEvent) {
			OnTrackPointAdditionEvent?.Invoke(trackPoint);
		}
	}
	protected virtual void RemovePoint(TrackPoint trackPoint, bool fireRemovalEvent = true) {
		this.anchorPoints.Remove(trackPoint);
		trackPath.RemoveAnchorPoint(trackPoint);
		if (fireRemovalEvent) {
			OnTrackPointRemovedEvent?.Invoke(trackPoint);
		}
	}
	public void Clear() {
		foreach (TrackPoint tp in new List<TrackPoint>(anchorPoints)) {
			if (tp.IsDeletable) {
				RemovePoint(tp);
			}
		}
	}

	public Vector2 GetClosestPointOnGrid(Vector2 rawPoint) {
		Vector2 gridPoint = new Vector2(
			Mathf.Round(rawPoint.x / this.trackResolution.x) * this.trackResolution.x,
			Mathf.Round(rawPoint.y / this.trackResolution.y) * this.trackResolution.y
		);
		return gridPoint;
	}

	public class TrackPoint {
		private Vector2 location;
		private LockDirection lockDirection;
		private bool isDeletable;

		public Func<float, float> StopInterpFunc;
		public Func<float, float> StartInterpFunc;

		public Vector2 Location => location;
		public bool IsEditable => lockDirection != LockDirection.All;
		public LockDirection Lock => lockDirection;
		public bool IsDeletable => isDeletable;

		public delegate void PreEditFunc(TrackPoint tp, ref Vector2 delta);
		public event PreEditFunc OnPreEdit;
		public event System.Action<TrackPoint, Vector2> OnEdit;
		public event System.Action<TrackPoint> OnNeedsRemoved;

		private Track.TrackPoint prevPoint;
		private Track.TrackPoint nextPoint;
		public Track.TrackPoint PrevPoint => prevPoint;
		public Track.TrackPoint NextPoint => nextPoint;

		public TrackPoint(Vector2 location, LockDirection lockDirection = LockDirection.None, bool isDeletable = true) {
			this.location = location;
			this.lockDirection = lockDirection;
			this.isDeletable = isDeletable;
		}

		public void Move(Vector2 delta) {
			Move(ref delta);
		}
		public void Move(ref Vector2 delta) {
			if (delta == Vector2.zero) {
				return;
			}
			OnPreEdit?.Invoke(this, ref delta);
			location += delta;

			if (location.x < prevPoint?.Location.x) {
				var oldPrev = prevPoint;
				var oldNext = nextPoint;
				var newPrev = prevPoint?.PrevPoint;

				newPrev?.SetNextPoint(this);
				oldPrev?.SetPreviousPoint(this);
				oldPrev?.SetNextPoint(oldNext);
				oldNext?.SetPreviousPoint(newPrev);

				SetNextPoint(oldPrev);
				SetPreviousPoint(newPrev);
			}
			else if (location.x > nextPoint?.Location.x) {
				var oldPrev = prevPoint;
				var oldNext = nextPoint;
				var newNext = nextPoint?.NextPoint;

				newNext?.SetPreviousPoint(this);
				oldNext?.SetNextPoint(this);
				oldNext?.SetPreviousPoint(oldPrev);
				oldPrev?.SetNextPoint(oldNext);

				SetPreviousPoint(oldNext);
				SetNextPoint(newNext);
			}
			OnEdit?.Invoke(this, delta);
		}
		public void SetLocation(Vector2 location) {
			this.location = location;
		}
		public void SetPreviousPoint(Track.TrackPoint point) {
			prevPoint = point;
		}
		public void SetNextPoint(Track.TrackPoint point) {
			nextPoint = point;
		}
		public void NotifyNeedsRemoved() {
			OnNeedsRemoved?.Invoke(this);
		}

		public enum LockDirection {
			None,
			Horizontal,
			Vertical,
			All
		}

		private void PrintSiblings() {
			Debug.Log($"{Location}: {prevPoint?.Location} -|- {nextPoint?.Location}");
		}
	}
}
