﻿using UnityEngine;

public abstract class TrackTimeTrigger : MonoBehaviour {
	private float currentTiming = 0.0f;
	protected TrackTriggerEvent[] triggerEvents;

	private System.Action onPlanningReaction;

	protected void Awake() {
		this.onPlanningReaction = () => {
			currentTiming = 0.0f;

			foreach (var triggerEvent in triggerEvents) {
				triggerEvent.Reset();
			}
		};
		GameStateManager.Instance.OnPlanningStateEnter += this.onPlanningReaction;
		triggerEvents = new TrackTriggerEvent[0];
	}

	protected void Update() {
		if (GameStateManager.Instance.CurrentGameState.State == GameStateManager.State.Executing) {
			currentTiming += Time.deltaTime;
			foreach (var triggerEvent in triggerEvents) {
				if (!triggerEvent.HasBeenInvoked) {
					if (currentTiming >= triggerEvent.TimeToTrigger) {
						triggerEvent.Invoke();
					}
				}
			}
		}
	}

	public void OnDestroy() {
		foreach (var triggerEvent in triggerEvents) {
			triggerEvent.Destroy();
		}
		if (GameStateManager.RawInstance) {
			GameStateManager.RawInstance.OnPlanningStateEnter -= this.onPlanningReaction;
		}
	}

	[System.Serializable]
	public abstract class TrackTriggerEvent {
		private float timeToTrigger;
		private event System.Action OnTriggerTrackEvent;
		private bool hasBeenInvoked;

		public float TimeToTrigger => timeToTrigger;
		public bool HasBeenInvoked => hasBeenInvoked;

		private System.Action destroyFunc;

		public TrackTriggerEvent(float timeToTrigger, System.Action trackTriggerEvent) {
			this.timeToTrigger = timeToTrigger;
			this.OnTriggerTrackEvent += trackTriggerEvent;
			this.destroyFunc = () => this.OnTriggerTrackEvent -= trackTriggerEvent;
			this.hasBeenInvoked = false;
		}

		public void Invoke() {
			OnTriggerTrackEvent?.Invoke();
			hasBeenInvoked = true;
		}

		public void Reset() {
			ResetTrigger();
			hasBeenInvoked = false;
		}
		protected abstract void ResetTrigger();

		public void Destroy() {
			this.destroyFunc();
		}
	}
}
